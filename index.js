const express = require('express');

const app = express();
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, x-access-token',
  );
  res.header(
    'Access-Control-Expose-Headers',
    'Content-Disposition, x-access-token',
  );

  if (req.method === 'OPTIONS') {
    res.status(200).send();
  } else {
    next();
  }
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('.'));

const PORT = 3000;

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.post('/response', (req, res) => {
  console.log('Freecharge response : ', req.body);
  if (req.body.status !== 'SUCCESS') {
    res.sendFile(__dirname + '/failure.html');
  } else {
    res.sendFile(__dirname + '/success.html');
  }
})

app.post('/callback', (req, res) => {
  console.log('Freecharge response body : ', req.body);
  res.status(200).send('data received');
});

app.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));
